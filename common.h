#ifndef _COMMON_H_
#define _COMMON_H_

#define SSL_SERVER_RSA_CERT 		"/var/james/certs/server.cert.pem"
#define SSL_SERVER_RSA_KEY 		    "/var/james/certs/server.key.pem"
#define SSL_SERVER_RSA_CA_CERT 		"/var/james/certs/ca.cert.pem"
#define SSL_SERVER_RSA_CA_PATH 		"/var/james/certs/"

#define SSL_CLIENT_RSA_CERT			"/var/james/certs/client.cert.pem"
#define SSL_CLIENT_RSA_KEY			"/var/james/certs/client.key.pem"
#define SSL_CLIENT_RSA_CA_CERT		"/var/james/certs/ca.cert.pem"
#define SSL_CLIENT_RSA_CA_PATH		SSL_SERVER_RSA_CA_PATH

#define OFF	0
#define ON	1

#define SERVER_IP                   "192.168.1.96"

#endif //_COMMON_H_
